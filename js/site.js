(function ($) {
    $(document).ready(function(){
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.navbar-default').css('min-height','50px');
                    $('.navbar-default').css('padding-top','0px');
                } else {
                    $('.navbar-default').css('min-height','100px');
                    $('.navbar-default').css('padding-top','25px');
                }
            });
        });

        $('body').scrollspy({target: ".navbar", offset: 0});
        $("#mainNavbar a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                scrollTop: $(hash).offset().top
                }, 800, function(){
                window.location.hash = hash;
                });
            }
        });
        $(".navbar-brand").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                scrollTop: $(hash).offset().top
                }, 500, function(){
                window.location.hash = hash;
                });
            }
        });
        
        $("#tagcloud a").tagcloud();
        $("#tagcloud a").tagcloud({
            size: {start: 1, end: 10, unit: "em"}

        });
        $("#tagcloud a").tagcloud({
            size: {start: 24, end: 72, unit: "px"},
            color: {start: '#ababab', end: '#000000'}
        });
    });
}(jQuery));